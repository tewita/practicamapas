package com.example.putex.incivisme;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class LlistarFragment extends Fragment {


    public LlistarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_incidencia, container, false);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();
        /**En las nuevas versiones de android lo que pasa es que salta del main al fragment, y este if lo utilizamos para controlar si el usuario es null, no inicie este fragmento*/
        if (auth.getUid()!= null) {
            DatabaseReference users = base.child("users");
            DatabaseReference uid = users.child(auth.getUid());
            DatabaseReference incidencies = uid.child("incidencies");


            FirebaseListOptions<Incidencia> options = new FirebaseListOptions.Builder<Incidencia>()
                    .setQuery(incidencies, Incidencia.class)
                    .setLayout(R.layout.fragment_listaincidencias)
                    .setLifecycleOwner(this)
                    .build();


            FirebaseListAdapter<Incidencia> adapter = new FirebaseListAdapter<Incidencia>(options) {
                @Override
                protected void populateView(View v, Incidencia model, int position) {
                    TextView txtDescripcio = v.findViewById(R.id.txtDescripcio);
                    TextView txtAdreca = v.findViewById(R.id.txtAdreca);

                    txtDescripcio.setText(model.getProblema());
                    txtAdreca.setText(model.getDireccio());
                }
            };

            ListView inci = view.findViewById(R.id.lvIncidencies);
            inci.setAdapter(adapter);

        }
        //commit
        return view;
    }}