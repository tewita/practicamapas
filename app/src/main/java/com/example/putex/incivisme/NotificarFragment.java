package com.example.putex.incivisme;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificarFragment extends Fragment {
    ProgressBar mLoading;
    private TextInputEditText txtLatitud;
    private TextInputEditText txtLongitud;
    private TextInputEditText txtDireccio;
    private TextInputEditText txtDescripcio;

    private Button buttonNotificar;
    private SharedViewModel model;

    //Ruta on guardarem la fotografia.
    String mCurrentPhotoPath;
    //Ruta a la fotografia en format URI.
    private Uri photoURI;
    //Utilitzarem aquesta variable per accedir al ImageView del fragment.
    private ImageView foto;
    //Aquesta variable la utilitzarem en el mètode startActivityForResult().
    static final int REQUEST_TAKE_PHOTO = 1;

    String downloadUrl;
    private StorageReference storageRef;

    public NotificarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificar, container, false);

        mLoading = view.findViewById(R.id.loading);

        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        txtLatitud =  view.findViewById(R.id.txtLatitud);
        txtLongitud = view.findViewById(R.id.txtLongitud);
        txtDireccio = view.findViewById(R.id.txtDireccio);
        txtDescripcio = view.findViewById(R.id.txtDescripcio);
        buttonNotificar = view.findViewById(R.id.button_notificar);


        model.getCurrentAddress().observe(this, address -> {
            txtDireccio.setText(getString(R.string.address_text,
                    address, System.currentTimeMillis()));
        });
        model.getCurrentLatLng().observe(this, latlng -> {
            txtLatitud.setText(String.valueOf(latlng.latitude));
            txtLongitud.setText(String.valueOf(latlng.longitude));
        });


        model.getProgressBar().observe(this, visible -> {
            if(visible)
                mLoading.setVisibility(ProgressBar.VISIBLE);
            else
                mLoading.setVisibility(ProgressBar.INVISIBLE);
        });

        model.switchTrackingLocation();



        buttonNotificar.setOnClickListener(button -> {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            storageRef = storage.getReference();

            StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
            UploadTask uploadTask = imageRef.putFile(photoURI);
            //Agreguem un Observer que es dispara quan acabi de pujar el fitxer.
            uploadTask.addOnSuccessListener(taskSnapshot -> {
                //taskSnapshot.getMetadata() contindrà metadades sobre com el fitxer com la mida, el tipus, etc.
                imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                    Uri downloadUri = task.getResult();
                    Glide.with(this).load(downloadUri).into(foto);

                    Log.e(null, downloadUri.toString());

                    downloadUrl = downloadUri.toString();


                    //Emplenem el POJO amb les dades del formulari.
                    Incidencia incidencia = new Incidencia();
                    incidencia.setDireccio(txtDireccio.getText().toString());
                    incidencia.setLatitud(txtLatitud.getText().toString());
                    incidencia.setLongitud(txtLongitud.getText().toString());
                    incidencia.setProblema(txtDescripcio.getText().toString());
                    incidencia.setUrl(downloadUrl); // Guardem la url de la imatge pujada.

                    //Instanciem el sistema d’autenticació.
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    //Demanem una referència a la BD.
                    DatabaseReference base = FirebaseDatabase.getInstance().getReference();

                    //Naveguem a la part d’usuaris.
                    DatabaseReference users = base.child("users");
                    //Busquem l’ID de l’usuari autenticat.
                    DatabaseReference uid = users.child(auth.getUid());
                    //Naveguem a la branca per a les notificacions de l’usuari.
                    DatabaseReference incidencies = uid.child("incidencies");

                    //Creem una incidència nova.
                    DatabaseReference reference = incidencies.push();
                    //Emplenem en les dades de l’objecte
                    reference.setValue(incidencia);

                    Toast.makeText(getContext(), "Avís donat " + downloadUrl, Toast.LENGTH_SHORT).show();
                });
            });
        });

        foto = view.findViewById(R.id.foto);
        Button buttonFoto = view.findViewById(R.id.button_foto);

        buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();
        });





        return view;
    }


    private File createImageFile() throws IOException {
        //Creem un nom de fitxer.
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                //Prefix.
                imageFileName,
                //Sufix.
                ".jpg",
                //Directori
                storageDir
        );
        //Guardem la imatge en la ruta completa.
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                //Verifiquem que hi ha alguna aplicació que pugui fer fotografies.
                getContext().getPackageManager()) != null) {
            //Creem el fitxer on es guardarà la foto.
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                //Gestionem els errors ocasionats.

            }

            //Continuem sols si no tenim errors.
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                //Creem l’Intent per a fer la foto.
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //Obrim la càmera del sistema. Al retornar s’executarà el mètode onActivityResult.
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Verifiquem que retornem des de la càmera.
        if (requestCode == REQUEST_TAKE_PHOTO) {
            //La foto s’ha realitzat correctament
            if (resultCode == Activity.RESULT_OK) {
                //Mostrem la imatge utilitzant Glide.

                Glide.with(this).load(photoURI).into(foto);



                //Aquest codi s’executarà si no s’ha fet la fotografia.
            } else {
                //Mostrem un missatge d’error.
                Toast.makeText(getContext(),
                        "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}